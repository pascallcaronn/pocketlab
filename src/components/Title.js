import React from 'react';

const Context = React.createContext(undefined);

export const CurrentTitleProvider = function(props) {
  return (
    <Context.Provider value={props.title} children={props.children || null} />
  );
};

export const withTitle = function(Component) {
  const displayName = `withTitle(${Component.displayName || Component.name})`;
  const C = props => {
    const {wrappedComponentRef, ...remainingProps} = props;
    return (
      <Context.Consumer>
        {context => (
          <Component
            {...remainingProps}
            {...context}
            ref={wrappedComponentRef}
          />
        )}
      </Context.Consumer>
    );
  };
  C.displayName = displayName;
  C.WrappedComponent = Component;
  return C;
};
