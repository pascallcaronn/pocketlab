import React, {useState, useEffect} from 'react';
import {FlatList, View} from 'react-native';
import {List, ActivityIndicator, Divider, Headline} from 'react-native-paper';
import {withRouter} from 'react-router-native';
import GitLabAPI from '../../../GitLabAPI';
import SyntaxHighlighter from 'react-native-syntax-highlighter';
import {atomOneLight} from 'react-syntax-highlighter/styles/hljs';

const Changes = props => {
  const {project, merge_request} = props.match.params;
  const [changes, setChanges] = useState(null);

  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab.mergeRequestChanges(project, merge_request).then(response => {
      setChanges(response.changes);
    });
  }, [merge_request, project]);

  if (changes === null) {
    return <ActivityIndicator animating={true} size="large" />;
  }
  return (
    <FlatList
      data={changes}
      keyExtractor={item => item.id}
      ItemSeparatorComponent={Divider}
      renderItem={({item}) => {
        return (
          <View>
            <Headline>{item.new_path}</Headline>
            <SyntaxHighlighter
              PreTag={View}
              CodeTag={View}
              language="diff"
              style={atomOneLight}>
              {item.diff}
            </SyntaxHighlighter>
          </View>
        );
      }}
    />
  );
};

export default withRouter(Changes);
