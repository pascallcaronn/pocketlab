import React, {useEffect, useState} from 'react';
import {Headline, BottomNavigation} from 'react-native-paper';
import GitLabAPI from '../../../GitLabAPI';
import {withTitle} from '@components/Title';
import Changes from './Changes';
import Overview from './Overview';
import Discussion from '@components/Discussion';

const routes = [
  {key: 'overview', title: 'Overview', icon: 'information-outline'},
  {key: 'discussion', title: 'Discussion', icon: 'forum'},
  {key: 'changes', title: 'Changes', icon: 'pencil-outline'},
];

const ViewMergeRequest = props => {
  const {project, merge_request, tab} = props.match.params;
  const selectedTab = routes.findIndex(element => element.key === tab);
  const [notes, setNotes] = useState([]);
  const [mergeRequest, setMergeRequest] = useState(null);

  const renderScene = BottomNavigation.SceneMap({
    overview: () => <Overview />,
    discussion: () => <Discussion notes={notes} />,
    changes: () => <Changes />,
  });

  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab.mergeRequest(project, merge_request).then(response => {
      props.setTitle(`${response.title} (!${response.iid})`);
      setMergeRequest(response);
    });
    gitlab.mergeRequestNotes(project, merge_request, 'asc').then(response => {
      setNotes(response);
    });
  }, [merge_request, project, props]);

  return (
    <BottomNavigation
      navigationState={{index: selectedTab, routes}}
      onIndexChange={newIndex => {
        console.log(
          `/projects/${project}/merge_requests/${merge_request}/${
            routes[newIndex].key
          }`,
        );
        props.history.replace(
          `/projects/${project}/merge_request/${merge_request}/${
            routes[newIndex].key
          }`,
        );
      }}
      renderScene={renderScene}
    />
  );
};

export default withTitle(ViewMergeRequest);
