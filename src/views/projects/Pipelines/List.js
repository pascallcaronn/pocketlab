import React, {useState, useEffect} from 'react';
import {StyleSheet, FlatList, View} from 'react-native';
import {
  List,
  Caption,
  ActivityIndicator,
  Divider,
  Colors,
} from 'react-native-paper';
import {withRouter} from 'react-router-native';
import GitLabAPI from '../../../GitLabAPI';
import moment from 'moment';
import statusMap from './StatusMap';
import * as Sentry from '@sentry/react-native';

const styles = StyleSheet.create({
  leftView: {
    width: 120,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  icon: {
    marginRight: 0,
  },
});

const ListPipelines = props => {
  const {params} = props.match;
  const {project} = params;
  const [pipelines, setPipelines] = useState(null);

  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab.pipelines(project).then(response => {
      if (!Array.isArray(response)) {
        response = [];
      }
      setPipelines(response);
    });
  }, [project]);

  if (pipelines === null) {
    return <ActivityIndicator animating={true} size="large" />;
  }

  return (
    <FlatList
      data={pipelines}
      ItemSeparatorComponent={Divider}
      keyExtractor={item => item.id}
      renderItem={({item, index}) => {
        let icon = statusMap[item.status];
        if (icon === undefined) {
          Sentry.captureMessage(
            `Pipeline has an unknown status "${item.status}"`,
          );
          icon = {icon: 'help-circle-outline', color: Colors.grey500};
        }
        return (
          <List.Item
            onPress={() =>
              props.history.push(`/projects/${project}/pipelines/${item.id}`)
            }
            title={`#${item.id}`}
            description={moment(item.updated_at).fromNow()}
            left={leftProps => (
              <View style={styles.leftView}>
                <List.Icon
                  {...leftProps}
                  color={icon.color}
                  style={styles.icon}
                  icon={icon.icon}
                />
                <Caption>{item.status}</Caption>
              </View>
            )}
          />
        );
      }}
    />
  );
};

export default withRouter(ListPipelines);
