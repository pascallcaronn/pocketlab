import React, {useState, useEffect} from 'react';
import {SectionList} from 'react-native';
import {List, ActivityIndicator, Divider, Colors} from 'react-native-paper';
import {withRouter} from 'react-router-native';
import GitLabAPI from '../../../GitLabAPI';
import moment from 'moment';
import statusMap from './StatusMap';
import * as Sentry from '@sentry/react-native';

const ViewPipeline = props => {
  const {project, pipeline} = props.match.params;
  const [jobs, setJobs] = useState(null);

  useEffect(() => {
    const gitlab = new GitLabAPI();
    gitlab.pipelineJobs(project, pipeline).then(response => {
      const stages = {};
      const output = [];
      if (Array.isArray(response)) {
        response.forEach(job => {
          if (stages[job.stage] === undefined) {
            stages[job.stage] = [];
          }
          stages[job.stage].push(job);
        });
        Object.keys(stages).map(stage => {
          output.push({
            title: stage,
            data: stages[stage],
          });
        });
      }
      setJobs(output);
    });
  }, [pipeline, project]);

  if (jobs === null) {
    return <ActivityIndicator animating={true} size="large" />;
  }
  return (
    <SectionList
      sections={jobs}
      keyExtractor={item => item.id}
      ItemSeparatorComponent={Divider}
      renderSectionHeader={({section: {title}}) => (
        <List.Subheader>{title}</List.Subheader>
      )}
      renderItem={({item}) => {
        let icon = statusMap[item.status];
        if (icon === undefined) {
          Sentry.captureMessage(
            `Pipeline has an unknown status "${item.status}"`,
          );
          icon = {icon: 'help-circle-outline', color: Colors.grey500};
        }
        return (
          <List.Item
            onPress={() =>
              props.history.push(`/projects/${project}/jobs/${item.id}`)
            }
            title={`${item.name} - ${item.status}`}
            description={moment(item.created_at).fromNow()}
            left={leftProps => (
              <List.Icon {...leftProps} color={icon.color} icon={icon.icon} />
            )}
          />
        );
      }}
    />
  );
};

export default withRouter(ViewPipeline);
