import {Colors} from 'react-native-paper';

const statusMap = {
  running: {icon: 'play-circle-outline', color: Colors.blue500},
  pending: {icon: 'dots-horizontal-circle-outline', color: Colors.blue500},
  success: {icon: 'check-circle-outline', color: Colors.green500},
  failed: {icon: 'close-circle-outline', color: Colors.red500},
  canceled: {icon: 'cancel', color: Colors.grey500},
  skipped: {icon: 'skip-next-circle-outline', color: Colors.grey500},
};

export default statusMap;
