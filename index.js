/**
 * @format
 */

import './shim.js';
import {AppRegistry, View} from 'react-native';
import React, {useState, useEffect} from 'react';
import {
  Provider as PaperProvider,
  Headline,
  Subheading,
  Button,
  ActivityIndicator,
} from 'react-native-paper';
import {NativeRouter, Redirect} from 'react-router-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import SafeAreaView from 'react-native-safe-area-view';
import App from './src/App';
import {name as appName} from './app.json';
import GitLabAPI from './src/GitLabAPI';
import AppSettings from '@modules/AppSettings';
import theme from './src/theme';
import {CurrentTitleProvider} from '@components/Title';
import * as Sentry from '@sentry/react-native';

if (process.env.NODE_ENV !== 'development') {
  Sentry.init({
    dsn: 'https://cec817e3271f4a579822795509a2cd12@sentry.io/1935730',
  });
}

const Wrapper: () => React$Node = () => {
  const [hasAccount, setHasAccount] = useState(null);
  const [title, setTitle] = useState('PocketLab');

  useEffect(() => {
    AppSettings.currentUser().then(user => {
      if (user !== undefined) {
        const gitlab = new GitLabAPI();
        gitlab.setConnection(user.host, user.accessToken);
      }
      setHasAccount(user !== undefined);
    });
  }, []);

  return (
    <PaperProvider theme={theme}>
      <NativeRouter>
        <CurrentTitleProvider title={{title, setTitle}}>
          <SafeAreaProvider>
            <SafeAreaView
              style={{flex: 1, backgroundColor: theme.colors.primary}}>
              {hasAccount === null && <ActivityIndicator />}
              {hasAccount !== null && <App />}
            </SafeAreaView>
          </SafeAreaProvider>
        </CurrentTitleProvider>
      </NativeRouter>
    </PaperProvider>
  );
};
AppRegistry.registerComponent(appName, () => Wrapper);
